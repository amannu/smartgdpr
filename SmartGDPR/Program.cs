﻿using FiveSpace;
using System;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace SmartGDPR
{
    class Program
    {
        private static bool paramError = false;
        private static string strLogText;


        static void Main(string[] args)
        {
            //Creo l'oggetto che esegue il log, adesso che ho il nome. Lo stesso vale per la DLL del database che usa quella dell'errore
            GlobalUtils.InitGestError();
            LogEvent(DateTime.Now, "Apertura dell'applicazione", "STARTING");

            if (args.Length != 4)
            {
                LogError(DateTime.Now, "Numero di parametri errato.");
                paramError = true;
            }
            else if (args[0] != "-c" && args[0] != "-d")
            {
                LogError(DateTime.Now, "Il primo parametro deve essere -c per criptare oppure -d per decriptare.");
                paramError = true;
            }
            else if(args[1].Length != 16)
            {
                LogError(DateTime.Now, "La password deve avere una lunghezza di 16 caratteri.");
                paramError = true;
            }
            else if(args[0] == "-c" && !Directory.Exists(args[2]))
            {
                LogError(DateTime.Now, "La cartella di origine non è stata trovata.");
                paramError = true;
            }
            else if (args[0] == "-d" && !File.Exists(args[2]))
            {
                LogError(DateTime.Now, "Il file di origine non è stato trovato.");
                paramError = true;
            }
            if(paramError)
            {
                LogEvent(DateTime.Now, "Chiusura dell'applicazione", "ENDING");
                return;
            }

            EncryptOrDecrypt(args[0], args[1], args[2], args[3]);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="type">-c per criptare, -d per decriptare</param>
        /// <param name="key">la chiave di cifratura</param>
        /// <param name="originPath">path della cartella di origine</param>
        /// <param name="destPath">path della cartella di destinazione</param>
        private static void EncryptOrDecrypt(string type, string key, string originPath, string destPath)
        {
            try
            {
                if (type == "-c") //Encrypt
                {
                    strLogText = "Cifratura";
                    var dirName = Path.GetFileName(originPath);
                    var tempFilePath = Path.Combine(Path.GetTempPath(), dirName);
                    tempFilePath += ".zip";

                    ExportToZip(originPath, tempFilePath);

                    var cryptFilePath = tempFilePath + ".crypt";
                    EncryptFile(tempFilePath, cryptFilePath, key);

                    var cryptFileName = dirName + ".zip" + ".crypt";
                    if(!Directory.Exists(destPath))
                    {
                        Directory.CreateDirectory(destPath);
                    }
                    else if (File.Exists(Path.Combine(destPath, cryptFileName)))
                    {
                        File.Delete(Path.Combine(destPath, cryptFileName));
                    }

                    File.Move(tempFilePath + ".crypt", Path.Combine(destPath, cryptFileName));
                    File.Delete(tempFilePath);
                }
                else //Decrypt
                {
                    strLogText = "Decifratura";
                    var fileName = Path.GetFileNameWithoutExtension(originPath);
                    if (!Directory.Exists(destPath))
                    {
                        Directory.CreateDirectory(destPath);
                    }
                    else if (File.Exists(Path.Combine(destPath, fileName)))
                    {
                        File.Delete(Path.Combine(destPath, fileName));
                    }

                    var destFilePath = Path.Combine(destPath, fileName);
                    DecryptFile(originPath, destFilePath, key);
                }

                LogEvent(DateTime.Now, strLogText + " eseguita con successo!");
            }
            catch (Exception e)
            {
                LogException(DateTime.Now, e);
            }
            finally
            {
                LogEvent(DateTime.Now, "Chiusura dell'applicazione", "ENDING");
            }
        }

        /// <summary>
        /// Esporta una directory in un file ZIP
        /// </summary>
        /// <param name="startPath">Il path della cartella da esportare</param>
        /// <param name="zipPath">il path dello zip</param>
        /// <returns></returns>
        private static void ExportToZip(string startPath, string zipPath)
        {
            if (File.Exists(zipPath))
                File.Delete(zipPath);
            ZipFile.CreateFromDirectory(startPath, zipPath, CompressionLevel.Fastest, true);
        }

        private static void EncryptFile(string inputFile, string outputFile, string password)
        {
            UnicodeEncoding UE = new UnicodeEncoding();
            byte[] key = UE.GetBytes(password);
            byte[] initVectorBytes = UE.GetBytes("12345678");

            string cryptFile = outputFile;
            FileStream fsCrypt = new FileStream(cryptFile, FileMode.Create);

            RijndaelManaged RMCrypto = new RijndaelManaged();

            CryptoStream cs = new CryptoStream(fsCrypt, RMCrypto.CreateEncryptor(key, initVectorBytes), CryptoStreamMode.Write);

            FileStream fsIn = new FileStream(inputFile, FileMode.Open);

            int data;
            while ((data = fsIn.ReadByte()) != -1)
                cs.WriteByte((byte)data);

            fsIn.Close();
            cs.Close();
            fsCrypt.Close();
        }

        private static void DecryptFile(string inputFile, string outputFile, string password)
        {
            UnicodeEncoding UE = new UnicodeEncoding();
            byte[] key = UE.GetBytes(password);
            byte[] initVectorBytes = UE.GetBytes("12345678");

            FileStream fsCrypt = new FileStream(inputFile, FileMode.Open);

            RijndaelManaged RMCrypto = new RijndaelManaged();

            CryptoStream cs = new CryptoStream(fsCrypt, RMCrypto.CreateDecryptor(key, initVectorBytes), CryptoStreamMode.Read);

            FileStream fsOut = new FileStream(outputFile, FileMode.Create);

            int data;
            while ((data = cs.ReadByte()) != -1)
                fsOut.WriteByte((byte)data);

            fsOut.Close();
            cs.Close();
            fsCrypt.Close();
        }


        #region ErrorLog

        private static void LogEvent(DateTime date, string text, string title = null)
        {
            Console.WriteLine(Environment.NewLine + date);
            if(title != null)
                Console.WriteLine(title);
            Console.WriteLine(text);
            Console.WriteLine();
            GlobalUtils.WriteLog(text, title);
        }

        private static void LogError(DateTime date, string text)
        {
            Console.WriteLine(Environment.NewLine + date);
            Console.WriteLine(text);
            Console.WriteLine();
            GlobalUtils.WriteError(text);
        }

        private static void LogException(DateTime date, Exception e)
        {
            Console.WriteLine(Environment.NewLine + date);
            Console.WriteLine(e.Message);
            Console.WriteLine();
            GlobalUtils.WriteException(e);
        }

        #endregion
    }
}
