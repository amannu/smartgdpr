﻿using System;

namespace FiveSpace
{
    public static class Const
    {
        /// <summary>
        /// Nome del programma corrente
        /// </summary>
        public static readonly string ASSEMBLY_NAME = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
        /// <summary>
        /// Path corrente della cartella dei documenti
        /// </summary>
        public static string DOCUMENT_PATH = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), @"5Space\" + ASSEMBLY_NAME + @"\");
        
    }
}
