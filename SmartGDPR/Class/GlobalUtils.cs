﻿using ErrorLog;
using System;

namespace FiveSpace
{
    public static class GlobalUtils
    {
        #region GlobalVar

        /// <summary>
        /// Contiene la gestione degli errori
        /// </summary>
        private static ErrorLog.ErrorLog GestErr;

        #endregion

        #region Chiamate ad ErrorLog

        /// <summary>
        /// Inizializza GestError
        /// </summary>
        public static void InitGestError()
        {
            GestErr = new ErrorLog.ErrorLog(Const.DOCUMENT_PATH, System.AppDomain.CurrentDomain.FriendlyName);
        }

        public static void WriteLog(string msg, string title = null)
        {
            try
            {
                if (title == null)
                    GestErr.WriteLine(msg);
                else
                    GestErr.WriteLine(msg, title);
            }
            catch (Exception)
            {
                /*Sotto try perché quando aggiorno la DLL ErrorLog.dll da problemi in quanto non riconosce la versione della DLL*/
            }
        }

        public static void WriteError(string error)
        {
            try
            {
                GestErr.Fail(error);
            }
            catch (Exception)
            {
                /*Sotto try perché quando aggiorno la DLL ErrorLog.dll da problemi in quanto non riconosce la versione della DLL*/
            }
        }

        public static void WriteException(Exception ex, string additionalInfo = null)
        {
            try
            {
                if (additionalInfo == null)
                    GestErr.WriteException(ex);
                else
                    GestErr.WriteException(ex, additionalInfo);
            }
            catch (Exception)
            {
                /*Sotto try perché quando aggiorno la DLL ErrorLog.dll da problemi in quanto non riconosce la versione della DLL*/
            }
        }

        #endregion
    }
}
